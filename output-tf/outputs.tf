# produces an output value named "container_id"
output "container_id" {
  description = "Terraform identifier of the Docker container (used internally)"
  value       = docker_container.nginx.id
}
# produces an output value named "image_id"
output "image_id" {
  description = "ID of the Docker image"
  value       = docker_image.nginx.image_id
}

